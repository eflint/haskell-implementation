These tests test the presence or absence of output rather than the validity of queries.
As a result, a successful run in --test-mode does not guarantee success.
