# eFLINT notebooks

[Jupyter notebooks](https://gitlab.com/eflint/jupyter/-/tree/master/notebooks) are available that explain the features and constructs of the eFLINT language.
