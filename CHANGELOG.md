# Revision history for package `eflint` 

## 3.1.0.0 (2023/04/22)  

* Started using changelog.

Last significant update: inclusion of 'instance queries' of the form `?- <EXPR>.`

## 3.1.0.1 (2023/04/24)

Added an additional form of instance query of the form `?-- <EXPR>.` as syntactic sugar for `?- <EXPR> When Holds(<EXPR>).`
